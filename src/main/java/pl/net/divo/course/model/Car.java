package pl.net.divo.course.model;

public class Car {
    private final String id;
    private final String name;
    private final String model;

    public Car(String id, String name, String model) {
        this.id = id;
        this.name = name;
        this.model = model;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }
}
